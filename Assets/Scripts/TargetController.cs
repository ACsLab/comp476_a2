﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    
    public enum Rules
    {
        Normal,
        Inverted
    }
    [SerializeField] private Rules rules = Rules.Normal;
    [SerializeField] private GameObject prefab;
    [SerializeField] private GameObject it;
    [SerializeField] private int NumberOfPlayers = 10;
    [SerializeField] private Transform[] spawnPositions;
    [SerializeField] public Material[] _materials = new Material[5];
    [SerializeField] private GameObject Astar_grid;

    private CustomGrid _grid;
    private List<GameObject> innocents = new List<GameObject>();
    private List<GameObject> seekers = new List<GameObject>();
    private List<GameObject> frozens = new List<GameObject>();
    private static TargetController instance;
    

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        float x = UnityEngine.Random.Range(-21.0f, 14.25f);
        float z = UnityEngine.Random.Range(-2.0f, 25.0f);
        _grid = Astar_grid.GetComponent<CustomGrid>();

        EntityController ec;
        for (int i = 0; i< NumberOfPlayers; i++)
        {
            
            //initilising all the entites
            if (rules == Rules.Normal)
            {
                innocents.Add(Instantiate(prefab, spawnPositions[i].position, new Quaternion()) as GameObject);
                ec = innocents[i].GetComponent<EntityController>();
                innocents[i].transform.parent = gameObject.transform;
                ec.State = EntityController.EntityStates.Wondering;
                ec.targetEntity = it;
                ec.grid = _grid;
                Debug.Log(i+ " : "+innocents[i].transform.position);
            }
            else
            {
                seekers.Add(Instantiate(prefab, spawnPositions[i].position, new Quaternion()) as GameObject);
                seekers[i].transform.parent = gameObject.transform;
                ec = seekers[i].GetComponent<EntityController>();
                ec.State = EntityController.EntityStates.Seeking;
                ec.targetEntity = it;
                ec.grid = _grid;
                Debug.Log(i+ " : "+seekers[i].transform.position);
            }
            
            
            x = UnityEngine.Random.Range(-22.0f, 15.25f);
            z = UnityEngine.Random.Range(-4.0f, 27.0f);

            //innocents.Add(a);
        }
        
        if (rules == Rules.Normal)
        {
            ec = innocents[0].GetComponent<EntityController>();
            //giving the tagger his first target
            it.GetComponent<EntityController>().targetEntity = innocents[0];
            it.GetComponent<EntityController>().grid = _grid;
            ec.targetEntity = it;
            ec.State = EntityController.EntityStates.Fleeing;
        }
        else
        {
            it.GetComponent<EntityController>().Chasers = seekers;
            it.GetComponent<EntityController>().grid = _grid;
            it.GetComponent<EntityController>().State = EntityController.EntityStates.Fleeing;

        }
        
    }
    //the tagger will request a random target from the target controller 
    public static GameObject RequestNewTarget(GameObject caller)
    {
        if (instance.innocents.Count < 1)
        {

            return null;
        }
        int i = UnityEngine.Random.Range(0, instance.innocents.Count);


        GameObject a = instance.innocents[i];

        a.GetComponent<EntityController>().State = EntityController.EntityStates.Fleeing;

        //Vector3 callerpos = caller.transform.position;

        //float smallestdist = Vector3.Distance(innocents[0].transform.position, callerpos);
        //float temp;
        //GameObject victim = innocents[0];

        //foreach (GameObject helper in innocents)
        //{
        //    temp = Vector3.Distance(helper.transform.position, callerpos);
        //    //if (Math.Abs(smallestdist.x) > Math.Abs(temp.x) && Math.Abs(smallestdist.z) > Math.Abs(temp.z))
        //    if (smallestdist > temp)
        //    {
        //        smallestdist = temp;
        //        victim = helper;
        //    }
        //}

        return a; 
    }
    //switches the caller from the frozen pool to the inncocent or alive pool
    public static void SwitchToActivePool(GameObject caller)
    {

        instance.innocents.Add(caller);
        instance.frozens.Remove(caller);
        
    }
    //a caller will request assistance from the nearest entity the entity will then change its state and move to assist
    public static void RequestAssistance(GameObject caller)
    {
        instance.frozens.Add(caller);
        instance.innocents.Remove(caller);
        

        Vector3 callerpos = caller.transform.position;
        if(instance.innocents.Count < 1)
        {
            return;
        }
        float smallestdist = Vector3.Distance(instance.innocents[0].transform.position, callerpos);
        float temp;
        GameObject savior = instance.innocents[0];

        foreach(GameObject helper in instance.innocents)
        {
            temp = Vector3.Distance(helper.transform.position, callerpos);
            //if (Math.Abs(smallestdist.x) > Math.Abs(temp.x) && Math.Abs(smallestdist.z) > Math.Abs(temp.z))
            if(smallestdist > temp && helper.GetComponent<EntityController>().State != EntityController.EntityStates.Fleeing)
            {
                smallestdist = temp;
                savior = helper;
            }
        }

        savior.GetComponent<EntityController>().Assisting(caller);


    }
    public static Material RequestNewMaterial(EntityController.EntityStates state)
    {
        switch (state)
        {
            case EntityController.EntityStates.Fleeing:
                return instance._materials[2];
                break;
            case EntityController.EntityStates.Wondering:
                return instance._materials[0];
                break;
            case EntityController.EntityStates.Frozen:
                return instance._materials[3];
            case EntityController.EntityStates.Assisting:
                return instance._materials[4];
            case EntityController.EntityStates.Seeking:
                return instance._materials[1];
            default:
                return instance._materials[0];
        }
        
            
        
           


    }

    public static Rules getRuleType()
    {
        return instance.rules;
    }

    //// Update is called once per frame
    //void Update()
    //{
        
    //}
}
