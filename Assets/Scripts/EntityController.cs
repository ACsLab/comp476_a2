﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EntityController : MonoBehaviour
{
    //different states a entity can be in 
    public enum EntityStates  {Seeking, Arriving, Wondering, Fleeing, Frozen, Assisting};
    public enum PathfindingType {NAVMESH, A_STAR, Free_Movement };   
    //the rolse a entity can fulfill 
    public enum EntityRole  {Tagger, Innocent};
    
    [SerializeField] private EntityStates currentState;
    public EntityStates State
    {
        get { return currentState; }
        set 
        {
            currentState = value;
            //_rend.sharedMaterial = TargetController.RequestNewMaterial(value);
            //Debug.Log("current state: " + currentState);
        }
    }

    [SerializeField] private EntityRole _role = EntityRole.Innocent;
    [SerializeField] private const float pathUpdateThreshold = 0.5f;
    [SerializeField] private const float minPathUpdateTime = 0.2f;
    [SerializeField] private float MaxVelocity = 5.0f;
    private GameObject _targetEntity;
    public GameObject targetEntity
    {
        get { return _targetEntity; }
        set
        {
            _targetEntity = value;
           
        }
    }//the entites current target
    public List<GameObject> Chasers;//the entites list of chasers if there are multiple
    private Vector3 chasersCenterOfMass;
    [SerializeField] private float ArrivalRadius = 5.0f;
    [SerializeField] private float SlowRadius = 25.0f;
    [SerializeField] private float FleeDistance = 10.0f;
    [SerializeField] private float navMeshWanderRadius;
    [SerializeField] private float navMeshWanderTime;
    [SerializeField] private PathfindingType _using = PathfindingType.NAVMESH;
    [SerializeField] private Renderer _rend;
    [SerializeField] private bool drawpath = true;

    public CustomGrid grid;
    private float _timer;
    private Rigidbody _rigidbody;
    private NavMeshAgent _navMeshAgent;
    //private TargetController _targetcontroller;
   

    //Vector3 currentSeekVelocity;

    private Vector3[] _path;
    Vector3 currentWaypoint;
    private int pathTargetIndex;
    private Vector3 WonderDestination = new Vector3(0,0,0);
    float enemyDistance;



    //when a value is changed in the inspector
    private void OnValidate()
    {
        
        State = currentState;
    }
    private void Awake()
    {
        //_grid = Astar_grid.GetComponent<CustomGrid>();
        _rigidbody = gameObject.GetComponent<Rigidbody>();
        _navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        
        _timer = navMeshWanderTime;
    }


    // Start is called before the first frame update
    void Start()
    {

        //Chasers = new List<GameObject>();

        _rend.sharedMaterial = TargetController.RequestNewMaterial(currentState);
        if (_using == PathfindingType.A_STAR)
            StartCoroutine(UpdatePath());

    }

    private void OnPathFound(Vector3[] newpath, bool pathSuccesfull)
    {

        if (pathSuccesfull)
        {
            _path = newpath;
            pathTargetIndex = 0;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");

        }
       
    }

    IEnumerator UpdatePath()
    {
        if (Time.timeSinceLevelLoad < 0.3f)
            yield return new WaitForSeconds(0.3f);

        
        float sqrthresh = pathUpdateThreshold * pathUpdateThreshold;
        //prevTargetpos = (currentState != EntityStates.Wondering) ? targetEntity.transform.position : WonderDestination;

        Vector3 prevTargetpos = (_role == EntityRole.Innocent)? DetermimTargetPositions() : targetEntity.transform.position;
        

        Vector3 currTargetpos = prevTargetpos;

        Vector3 thisEntityPos = this.transform.position;
        Vector3 dirAwayFromTarget = thisEntityPos - currTargetpos;

        if (currentState == EntityStates.Fleeing)
            //need to change end position if fleeing
            PathRequestManager.RequestPath(new PathRequest(transform.position, thisEntityPos + (dirAwayFromTarget * MaxVelocity), OnPathFound));
        else if (currentState == EntityStates.Wondering)
           
            PathRequestManager.RequestPath(new PathRequest(transform.position, WonderDestination, OnPathFound));
        else
            PathRequestManager.RequestPath(new PathRequest(transform.position, currTargetpos, OnPathFound));

        while (true)
        {
            yield return new WaitForSeconds(minPathUpdateTime);
            currTargetpos = (_role == EntityRole.Innocent) ? DetermimTargetPositions() : targetEntity.transform.position;
            if ((currTargetpos - prevTargetpos).sqrMagnitude > sqrthresh)
            {
                thisEntityPos = this.transform.position;
                dirAwayFromTarget = thisEntityPos - currTargetpos;

                if (currentState == EntityStates.Fleeing)
                    //need to change end position if fleeing
                    PathRequestManager.RequestPath(new PathRequest(transform.position, thisEntityPos + (dirAwayFromTarget * MaxVelocity), OnPathFound));
                else if (currentState == EntityStates.Wondering)
                    PathRequestManager.RequestPath(new PathRequest(transform.position, WonderDestination, OnPathFound));
                else
                    PathRequestManager.RequestPath(new PathRequest(transform.position, currTargetpos, OnPathFound));
                prevTargetpos = currTargetpos;

            }
        }
    }

    IEnumerator FollowPath()
    {
        

        currentWaypoint = _path[0];

        while (true)
        {
            if (grid.NodeFromWorldPoint(transform.position) == grid.NodeFromWorldPoint(currentWaypoint))
            {
                pathTargetIndex++;
                if (pathTargetIndex >= _path.Length)
                {
                    yield break;
                }
                currentWaypoint = _path[pathTargetIndex];
                //currentWaypoint = new Vector3(currentWaypoint.x, this.transform.position.y, currentWaypoint.z);
            }
            //else
            //{
            //    print(transform.position + " : " + currentWaypoint);
            //}
            
            //do movement here
            Vector3 currentSeekVelocity = GetSeekVelocity(transform.position, currentWaypoint, false);
            transform.position = this.transform.position + currentSeekVelocity;
            //transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, 2 * Time.deltaTime);

            yield return null;

        }

    }

        // Update is called once per frame
    void Update()
    {
        //execute a certain action depending on what the enetites state is
        switch (currentState)
        {
            case EntityStates.Seeking:
                Seek(false);
                break;
            case EntityStates.Arriving:
                Arriving();
                break;
            case EntityStates.Fleeing:
                Fleeing();
                break;
            case EntityStates.Wondering:
                Wondering();
                break;
            case EntityStates.Assisting:
                Seek(false);
                break;
            case EntityStates.Frozen:
                break;
        }
    }

    private Vector3 GetSeekVelocity(Vector3 currentposition, Vector3 target, bool fleeing)
    {
        
        Vector3 posdiff = target - currentposition;
        Vector3 absposdiff = new Vector3(Math.Abs(posdiff.x), Math.Abs(posdiff.y), Math.Abs(posdiff.z));
        float theta = 1e-5f; 
        Vector3 am = new Vector3(posdiff.x / (absposdiff.x + theta), 0, posdiff.z / (absposdiff.z + theta));

        return fleeing ? _rigidbody.velocity + MaxVelocity * -am * Time.deltaTime : _rigidbody.velocity + MaxVelocity * am * Time.deltaTime;
    }

	/*
     * helper method to find which seekers the intity can see
     */
    bool SeekerSeen(out Vector3 seekerpos)
    {

        Vector3 direction = new Vector3();

        direction = this.transform.forward;

        RaycastHit hit;
        
        
        if (Physics.SphereCast(this.transform.position, 1.0f, direction, out hit, FleeDistance + 1))
        {
            if (hit.collider.CompareTag("Player"))
            {
                seekerpos = hit.transform.position;
                targetEntity = hit.collider.gameObject;
                print("spotted");
                return true;
            }
        }
        
        seekerpos = new Vector3();
        return false;
    }
    
    /*
     * helper method
     * calulates center of "mass" between the seekers
     */
    Vector3 CenterOfMass()
    {
       
        Vector3 position = new Vector3(0.0f, 0.0f, 0.0f);

        float pX=0;
        float pZ=0;
        

        foreach (GameObject obj in Chasers)
        {
            position += obj.transform.position;
             
        }

        position /= Chasers.Count;

      
        return position;
    }

    /*
     *Steering seek
     * move towards a target using the steering seek calculation
     * if bool flee is set to true the entity will face run away from the tagger
     */
    void Seek(bool flee)
    {


        if (targetEntity == null)
        {
            return;
        }
        Vector3 currentPosition = this.transform.position;
        Vector3 targetPosition = targetEntity.transform.position;
        //Vector3 posdiff = targetPosition - currentPosition;
        //Vector3 absposdiff = new Vector3(Math.Abs(posdiff.x), Math.Abs(posdiff.y), Math.Abs(posdiff.z));

        //Vector3 am = new Vector3(posdiff.x / absposdiff.x, 0, posdiff.z / absposdiff.z);



        //currentSeekVelocity = flee ? _rigidbody.velocity + MaxVelocity * -am * Time.deltaTime : _rigidbody.velocity + MaxVelocity * am * Time.deltaTime;
        Vector3 currentSeekVelocity = GetSeekVelocity(currentPosition, targetPosition, flee);
        Vector3 dirtoplayer = currentPosition - targetPosition;

        switch (_using)
        {
            case PathfindingType.Free_Movement:
                transform.position = currentPosition + currentSeekVelocity;
                break;
            case PathfindingType.NAVMESH:
                if (flee)
                    _navMeshAgent.SetDestination(currentPosition + dirtoplayer);
                else
                    _navMeshAgent.SetDestination(targetPosition);
                break;
            case PathfindingType.A_STAR:
                //if (flee)
                //    PathRequestManager.RequestPath(new PathRequest(transform.position, currentPosition + dirtoplayer, OnPathFound));
                //else
                //    PathRequestManager.RequestPath(new PathRequest(transform.position, targetEntity.transform.position, OnPathFound));
                break;
        }

        updateOrientation(flee);
        


        Vector3 targetRadius = new Vector3(SlowRadius+targetPosition.x,0,SlowRadius + targetPosition.y);
        //Debug.Log("slow radius: "+ Math.Abs(targetRadius.x)+" position: "+ currentPosition);
        float radiusarrive = targetEntity.GetComponent<EntityController>().SlowRadius;

        State = Math.Pow(currentPosition.x - targetPosition.x, 2) + Math.Pow(currentPosition.y - targetPosition.y, 2) <= Math.Pow(radiusarrive, 2) && !flee
            ? EntityStates.Arriving: currentState;

    }
     /*
      *steering arrive 
      * once the entity has reached the slow radius its state will change to arriving 
      * where the steering arrive calculation will be performed untill the tagger reaches the 
      * arrivale radius where he will "tag" or freeze the victim 
      * 
      */
    private void Arriving()
    {
        if (targetEntity == null)
        {
            return;
        }
        Vector3 currentPosition = this.transform.position;
        float radiusarrive = targetEntity.GetComponent<EntityController>().ArrivalRadius;
        float radiusslow = targetEntity.GetComponent<EntityController>().SlowRadius;
        Vector3 targetPosition = targetEntity.transform.position;
        Vector3 targetRadius = new Vector3(radiusarrive+targetPosition.x, 0, radiusarrive + targetPosition.y);


        //check if still in slow down radius
        State = Math.Pow(currentPosition.x - targetPosition.x, 2) + Math.Pow(currentPosition.y - targetPosition.y, 2) > Math.Pow(radiusslow, 2) ? EntityStates.Seeking : currentState;

        float distance = Vector3.Distance(targetEntity.transform.position, transform.position);
        //double distance = Math.Pow(currentPosition.x - targetPosition.x, 2) + Math.Pow(currentPosition.y - targetPosition.y, 2);
        //check if inside arrival radius
        if (distance <= Math.Pow(radiusarrive,2))
        {

            //stop the tagger before picking a new target 
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;

            Tagged();
            

            return;
        }



        Vector3 posdiff = targetPosition - currentPosition;
        Vector3 goalVelocity =  new Vector3(posdiff.x / SlowRadius,0,posdiff.z / SlowRadius);
        
        Vector3 accel = (goalVelocity - _rigidbody.velocity)*MaxVelocity/0.6f;

        
        switch (_using)
        {
            case PathfindingType.Free_Movement:
                transform.position = currentPosition + accel * Time.deltaTime;
                break;
            case PathfindingType.NAVMESH:
                _navMeshAgent.SetDestination(targetPosition);
                break;
            case PathfindingType.A_STAR:
                //PathRequestManager.RequestPath(new PathRequest(transform.position, targetEntity.transform.position, OnPathFound));

                break;

        }



        updateOrientation(false);
    }

    private void Wondering()
    {

        switch (_using)
        {
            case PathfindingType.Free_Movement:
                Vector3 currentPosition = this.transform.position;
                Vector3 randomlookat = new Vector3(0, UnityEngine.Random.Range(-1.0f, 1.0f), 0);
                transform.Rotate(randomlookat);
                Vector3 forward = transform.TransformDirection(Vector3.forward);
                transform.position = currentPosition + _rigidbody.velocity + transform.forward * MaxVelocity * Time.deltaTime;
                break;
            case PathfindingType.NAVMESH:
                navMeshWonder();
                break;
            case PathfindingType.A_STAR:
                AStarWonder();
                break;
        }


        

    }

    Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * dist;

        randomDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randomDirection, out navHit, dist, layermask);

        return navHit.position;

    } 

    private void navMeshWonder()
    {
        _timer += Time.deltaTime;

        if (_timer >= navMeshWanderTime)
        {
            Vector3 newPos = RandomNavSphere(transform.position, navMeshWanderRadius, -1);
            _navMeshAgent.SetDestination(newPos);
            _timer = 0;
        }

        updateOrientation(false);
    }

    private void AStarWonder()
    {
        _timer += Time.deltaTime;

        if (_timer >= navMeshWanderTime)
        {
            int gridrowsize = grid.grid.GetLength(0);
            int gridcolsize = grid.grid.GetLength(1);

            int x = UnityEngine.Random.Range(0, gridrowsize);
            int y = UnityEngine.Random.Range(0, gridcolsize);
            GridNode targetNode = grid.grid[x, y];
            while (!targetNode.walkable)
            {

                x = UnityEngine.Random.Range(0, gridrowsize);
                y = UnityEngine.Random.Range(0, gridcolsize);

                targetNode = grid.grid[x, y];
            }
         
            WonderDestination = targetNode.worldPosition;
            _timer = 0;
        }

        updateOrientation(false);
    }
    
    void Fleeing()
    {

        //if there are multiple chasers
        //if(Chasers != null)
        //{

        //        //if i spot a chaser do something
        //        //else do something else
        //}
        //else
        //{
        //    //if there is only one giving chase
        //    //do regular thing
        //}
        //float enemyDistance = Vector3.Distance(targetEntity.transform.position , transform.position );
        //check if there are multiple chasers 
        //if yes then calculate th center of mass and use that as distance and direction to flee from
        //if not run from single target
        //when running away from a target and get stuck try to get unstuck
        //or choose a path that dose not get you stuck 
        //TargetController.currentRules;


        if(Chasers == null)
            enemyDistance = Vector3.Distance(targetEntity.transform.position, transform.position);
        else
        {
            chasersCenterOfMass = CenterOfMass();
            enemyDistance = Vector3.Distance(chasersCenterOfMass, transform.position);
        }
        //enemyDistance = (Chasers == null) ? Vector3.Distance(targetEntity.transform.position, transform.position) : Vector3.Distance(CenterOfMass(), transform.position);
        Vector3 seekerpos;
        if (SeekerSeen(out seekerpos) && Chasers != null)
            enemyDistance = Vector3.Distance(seekerpos, transform.position);


        switch (_using)
        {
            case PathfindingType.Free_Movement:
                //enemyDistance = Vector3.Distance(targetEntity.transform.position, transform.position);
                if (enemyDistance < FleeDistance)
                    Seek(true);
                else
                    Wondering();
                break;
            case PathfindingType.NAVMESH:
                //enemyDistance = Vector3.Distance(targetEntity.transform.position, transform.position);
                if (enemyDistance < FleeDistance)
                    Seek(true);
                else
                    navMeshWonder();
                break;
            case PathfindingType.A_STAR:
                //enemyDistance = Vector3.Distance(targetEntity.transform.position, transform.position);
                if (enemyDistance < FleeDistance)
                    Seek(true);
                else
                    AStarWonder();
                break;
        }

        

       
        
    }

    private Vector3 DetermimTargetPositions()
    {
        Vector3 prevpos;
        
        if (TargetController.getRuleType() == TargetController.Rules.Normal)
            prevpos = (currentState != EntityStates.Wondering) ? targetEntity.transform.position : WonderDestination;
        else
        {
            Vector3 seekerpos;
            prevpos = (currentState != EntityStates.Wondering) ? chasersCenterOfMass : WonderDestination;
            prevpos = SeekerSeen(out seekerpos) ? seekerpos : prevpos;
        }

        return prevpos;
    }
    //method to change the orientation of a entity if lookayaw is set to true then the entity will look in the opposite direction
    private void updateOrientation(bool lookaway)
    {

        Vector3 currentPosition = transform.position;

        Quaternion targetRotation = Quaternion.LookRotation(_rigidbody.velocity);
        targetRotation = _using == PathfindingType.NAVMESH ? Quaternion.LookRotation(_navMeshAgent.velocity) : targetRotation;
        targetRotation = _using == PathfindingType.A_STAR ? Quaternion.LookRotation(currentWaypoint - currentPosition) : targetRotation;
        targetRotation = lookaway ? Quaternion.LookRotation(currentPosition - targetEntity.transform.position) : targetRotation;

        float strength = Mathf.Min((float)(MaxVelocity * Time.deltaTime), 1.0f);
        
        //transform.Rotate(_rigidbody.angularVelocity * Time.deltaTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, strength);
    }

    //function that stops an entity and changes itsa state to frozen
    private void freeze()
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
        _navMeshAgent.isStopped = true;
        StopAllCoroutines();
        /*
         *change the entities color to signify frozen ness
         */
        currentState = EntityStates.Frozen;
        _rend.sharedMaterial = TargetController.RequestNewMaterial(currentState);
        
        TargetController.RequestAssistance(gameObject);

        
    }
    //helper method to unfreeze an entity 
    private void unfreeze()
    {
        currentState = EntityStates.Wondering;
        _rend.sharedMaterial = TargetController.RequestNewMaterial(currentState);

        //put bag in innocent pool
        TargetController.SwitchToActivePool(gameObject);

    }
    /*
     * depending on the entites role tag will perform one of 2 actions:
     * 
     * either freeze the target if the current entity is a tagger 
     * the find a new target 
     * 
     * or
     * 
     * unfreeze the target if the entity is labeled as innocent
     * then change its state back to wonder and continue wondering
     * 
     */
    private void Tagged()
    {
        switch (_role)
        {
            case EntityRole.Tagger:
                targetEntity.GetComponent<EntityController>().freeze();
                targetEntity = TargetController.RequestNewTarget(gameObject);
                if (targetEntity != null)
                {

                    targetEntity.GetComponent<EntityController>().targetEntity = this.gameObject;
                }
                State = EntityStates.Seeking;
                break;
            case EntityRole.Innocent:
                targetEntity.GetComponent<EntityController>().unfreeze();
                State = EntityStates.Wondering;
                _rend.sharedMaterial = TargetController.RequestNewMaterial(currentState);
                break;
        }
    }

    //helper function that is used by an entity who is going to unfreeze a target
    //sets the cuurent entites state to assisting
    public void Assisting(GameObject caller)
    {
        targetEntity = caller;

        State = EntityStates.Assisting;

        _rend.sharedMaterial = TargetController.RequestNewMaterial(currentState);

    }

   

    public void OnDrawGizmos()
    {
        if (_path != null && drawpath)
        {
            for (int i = pathTargetIndex; i < _path.Length; i++)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube(_path[i] + Vector3.up, Vector3.one * (0.3f - 0.1f));

                if (i == pathTargetIndex)
                {
                    Gizmos.DrawLine(transform.position, _path[i]);
                }
                else
                {
                    Gizmos.DrawLine(_path[i - 1], _path[i]);
                }
            }
        }
    }


}
