﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomGrid : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private LayerMask unwalkableMask;
    [SerializeField] private Vector2 gridWorldSize = new Vector2(10,10);
    [SerializeField] private float nodeRadius;
    [SerializeField] private int nonWalkableAdjacencyPenalty = 2;
    [SerializeField] private bool displayGizmos = false;

    private GridNode[,] _grid;
    public GridNode[,] grid { get { return _grid; } } 

    private float nodeDiameter;
    private int gridSizeX, gridSizeY;

    public int MaxSize
    {
        get { return gridSizeX * gridSizeY; }
    }

    private void Awake()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

    /// <summary>
    /// creates a grid and stores it in a heap for better efficiency
    /// </summary>
    private void CreateGrid()
    {
        _grid = new GridNode[gridSizeX, gridSizeY];

        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (i * nodeDiameter + nodeRadius) + Vector3.forward * (j * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                int movemntpenalty = 0;
                _grid[i, j] = new GridNode(walkable, worldPoint, new Vector2(i, j), movemntpenalty);
            }
        }

        foreach (GridNode node in _grid)
        {
            List<GridNode> neighbours = GetNeighbours(node);
            foreach (GridNode neighbour in neighbours)
            {
                if (!neighbour.walkable)
                {
                    node.movementPenalty = nonWalkableAdjacencyPenalty;
                    break;
                }
            }

        }
    }
     ///gets the neighbours of a GridNode     
    public List<GridNode> GetNeighbours(GridNode node)
    {
        List<GridNode> neighbours = new List<GridNode>();

        for(int i = -1; i <= 1; i++)
        {
            for(int j = -1; j<= 1; j++)
            {
                if (i == 0 && j == 0)
                {
                    continue;
                }
                int checkx = (int) node.gridposition.x + i;
                int checky = (int) node.gridposition.y + j;

                if(checkx >= 0 && checkx < gridSizeX && checky >=0 && checky < gridSizeY)
                {
                    neighbours.Add(_grid[checkx, checky]);
                }


            }
        }


        return neighbours;

    }
    //returns a node from a world coordinate 
    public GridNode NodeFromWorldPoint(Vector3 worldposition)
    {
        float percentx = (worldposition.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percenty = (worldposition.z + gridWorldSize.y / 2) / gridWorldSize.y;
        percentx = Mathf.Clamp01(percentx);
        percenty = Mathf.Clamp01(percenty);

        int x = Mathf.RoundToInt((gridSizeX -1) * percentx);
        int y = Mathf.RoundToInt((gridSizeY -1) * percenty);

        if(x > _grid.GetLength(0) || y > _grid.GetLength(1))
        {
            return _grid[_grid.GetLength(0), _grid.GetLength(1)];
        }

        return _grid[x, y];
    }

    

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        if (_grid != null && displayGizmos)
        {
            GridNode playerNode = NodeFromWorldPoint(player.position);
            

            foreach(GridNode n in _grid)
            {
                Gizmos.color = n.walkable ? Color.white : Color.red;

              
                if (playerNode == n)
                {
                    Gizmos.color = Color.green;
                }
                Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter-0.1f));
            }
        }


    }
}
