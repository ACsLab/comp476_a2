﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class PathRequestManager : MonoBehaviour
{

    
    Queue<PathResult> results = new Queue<PathResult>();
    static PathRequestManager instance;

    Pathfinding pathfinding;


    /// <summary>
    /// starts a new thread for a pathfinding request 
    /// </summary>
    /// <param name="request"></param>
    public static void RequestPath(PathRequest request)
    {
        ThreadStart threadstart = delegate
        {
            instance.pathfinding.FindPath(request, instance.FinishedProcessingPath);
        };

        threadstart.Invoke();


    }

    /// <summary>
    /// queues the result of the process 
    /// waiting for the main thread to process the callback
    /// </summary>
    /// <param name="result"></param>
    public void FinishedProcessingPath(PathResult result)
    {

        lock (results)
        {
            results.Enqueue(result);
        }


    }
   
    private void Awake()
    {
        instance = this;
        pathfinding = GetComponent<Pathfinding>();
    }
   
    // Update is called once per frame
    void Update()
    {
        if (results.Count > 0)
        {
            int itemsInQueue = results.Count;
            lock (results)
            {
                for (int i = 0; i < itemsInQueue; i++)
                {
                    PathResult result = results.Dequeue();
                    result.callback(result.path, result.success);
                }
            }
        }
    }


}
public struct PathResult
{
    public Vector3[] path;
    public bool success;
    public Action<Vector3[], bool> callback;

    public PathResult(Vector3[] path, bool success, Action<Vector3[], bool> callback) {

        this.path = path;
        this.success = success;
        this.callback = callback;

    }
}
/// <summary>
/// a struct used to request a a path from the path request manager with a callback function to the original caller
/// the call back returns a vector 3 array of path positions and a bool denoting the succes or failure of finding a path 
/// </summary>
public struct PathRequest
{
    public Vector3 pathStart;
    public Vector3 pathEnd;
    public Action<Vector3[], bool> callback;

    public PathRequest(Vector3 _start, Vector3 _end, Action<Vector3[], bool> _callback)
    {
        pathStart = _start;
        pathEnd = _end;
        callback = _callback;

    }
}
