﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PointNode : MonoBehaviour
{
    private int _index = -1;
  
    private Node<Vector3> _node;
    //private Transform _transform;
    public Node<Vector3> Node { get { return _node; } }
    //public Transform PointTransform { get {return _transform;}}

    // Start is called before the first frame update
    void Start()
    {
        
        //_transform = this.transform;
        //_connections = new List<Node<Transform>>();
        _node = new Node<Vector3>(transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
