﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


public class POVGraph : MonoBehaviour
{
    
    
    [SerializeField]private GameObject points;
    [SerializeField]private LayerMask mask;
    //private PointNode[] _pointPositions;
    private GenericTree<Transform> _posTree;
    private List<PointNode> _graph;

    private GenericTree<Transform> MakeTree(Transform[] positions)
    {
        //take first point raycast add hits as children 
        //take those children raycast from them add hits as children 
        //repeat...
        //GenericTree<Transform>
        //

        int layerMask = 1 << 9;//used to target objects exclusively on this layer 
        RaycastHit hit;
        _posTree = new GenericTree<Transform>(positions[1]);//the tree with the root position[0] is wrong atm   
        Collider[] neighbours = Physics.OverlapSphere(positions[1].position, 6.0f, layerMask);//find neighbour colliders on the 9th layer
        Transform origin;
        
       
        int nodeindex = 0;
        origin = _posTree[nodeindex].Data;
        while (true)
        {

            
            
            for(int i = 0; i< neighbours.Length; i++)
            {
                Physics.Raycast(origin.position, neighbours[i].transform.position - origin.position, out hit, 10.0f);//cast ray in direction of colliders from origin
            
                if(hit.collider !=null)//check if collider is not the origins colider
                {
                    if (hit.collider.tag == "Point")//check if the colider tag is a point (all points of interest should be marked as points)
                    {
                        Debug.DrawLine(origin.position, hit.transform.position, Color.blue, 15.0f);//display the line for debuging purposes
                        hit.collider.enabled = false;//disable the collider of the point so it wont be hit by another ray
                                                     //childlist.Add(hit);
                        _posTree.addchildto(nodeindex, hit.transform);//add child to node
                    }
                    
                }
                
            }
            
            nodeindex++;//go to next leaf
            if (nodeindex >= _posTree.Count) break;//check if index is greater than number of nodes
            origin = _posTree[nodeindex].Data;//set leaf as new origin to cast ray

            neighbours = Physics.OverlapSphere(origin.position, 6.0f, layerMask);//get colliders in viscinity


        }


        return _posTree;

    }

    private List<PointNode> MakeGraph(PointNode[] positions)
    {
        List<PointNode> _nodes = new List<PointNode>();

        RaycastHit hit;
        //int layerMask = 1 << 9;//used to target objects exclusively on this layer 
        Collider[] neighbours = Physics.OverlapSphere(positions[0].Node.Data, 6.0f, mask);
        PointNode origin;
        
        origin = positions[0];
        int nodeindex = 0;
        _nodes.Add(origin);
        while (true)
        {

            for(int j = 0; j< neighbours.Length; j++)
            {
                int hitindexes = 0;
                Physics.Raycast(origin.Node.Data, neighbours[j].transform.position - origin.Node.Data, out hit, 10.0f, mask);//cast ray in direction of colliders from origin
            
                if(hit.collider !=null)//check if collider is not the origins colider
                {
                    hitindexes++;
                    if (origin.Node.Parent == null)
                    {
                        if (hit.collider.tag == "Point")
                        {
                            Debug.DrawLine(origin.Node.Data, hit.transform.position, Color.red, 15.0f);//display the line for debuging purposes
                            origin.Node.addchild(hit.collider.gameObject.GetComponent<PointNode>().Node);
                            _nodes.Add(hit.collider.gameObject.GetComponent<PointNode>());

                        }
                        
                    }
                    else if (hit.collider.tag == "Point" && hit.collider.gameObject.GetComponent<PointNode>().Node.Data != origin.Node.Parent.Data)//check if the colider tag is a point (all points of interest should be marked as points)
                    {
                        Debug.DrawLine(origin.Node.Data, hit.transform.position, Color.red, 15.0f);//display the line for debuging purposes
                        //hit.collider.enabled = false;//disable the collider of the point so it wont be hit by another ray
                        //childlist.Add(hit);
                        //_posTree.addchildto(nodeindex, hit.transform);//add child to node
                        origin.Node.addchild(hit.collider.gameObject.GetComponent<PointNode>().Node);
                        _nodes.Add(hit.collider.gameObject.GetComponent<PointNode>());
                    }
                    
                }
                
            }

            nodeindex++;
            if (nodeindex >= positions.Length) break;
            origin = positions[nodeindex];
            neighbours = Physics.OverlapSphere(origin.Node.Data, 8.0f, mask);//get colliders in viscinity

        }
        

        return _nodes;

    }

    private IEnumerator coroutine;
    // Start is called before the first frame update
    private IEnumerator WaitForData(PointNode[] pointpositions)
    {
        while (true)
        {
            if (pointpositions[0].Node != null)
            {
                print("data" + pointpositions[0].Node.Data);
                _graph = MakeGraph(pointpositions);
                yield break;
            }
            else
            {
                yield return new WaitForSeconds(0.01f);
                print("waited");
            }
        }
        
    }
    void Start()
    {
        PointNode[] _pointPositions = points != null ? points.GetComponentsInChildren<PointNode>() : new PointNode[1];

        coroutine = WaitForData(_pointPositions);
        StartCoroutine(coroutine);

       

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
