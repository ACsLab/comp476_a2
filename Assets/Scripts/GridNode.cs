﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridNode : IHeapItem<GridNode>
{
    public bool walkable;
    public Vector3 worldPosition;
    public Vector2 gridposition;
    public GridNode parent;
    private int heapIndex;
    public int HeapIndex { get { return heapIndex; } set { heapIndex = value; } }

    public int movementPenalty;
    public int Gcost;
    public int hcost;
    public int fcost
    {
        get { return Gcost + hcost; }
    }


    /// <summary>
    /// a grid node that holds its world position and its position on the grid
    /// to be used with the Custom Grid classs and create a grid for uise with the A* algorithm
    /// implements the IHeapItem interface so that it can be store in a heap
    /// </summary>
    /// <param name="_walkable"></param>
    /// <param name="worldpos"></param>
    /// <param name="gridpos"></param>
    /// <param name="movementpenalty"></param>
    public GridNode(bool _walkable, Vector3 worldpos, Vector2 gridpos, int movementpenalty)
    {
        movementPenalty = movementpenalty;
        walkable = _walkable;
        worldPosition = worldpos;
        gridposition = gridpos;
    }

    public int CompareTo(GridNode other)
    {
        int compare = fcost.CompareTo(other.fcost);
        if (compare == 0)
        {
            compare = hcost.CompareTo(other.hcost);
        }

        return -compare;
    }
}
