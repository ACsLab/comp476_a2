﻿using System.Collections;
using System.Collections.Generic;
using System;
/// <summary>
/// generic heap class that contains items with the IHeapItem Interface 
/// is able to sort down and up 
/// </summary>
/// <typeparam name="T"></typeparam>
public class Heap<T> where T : IHeapItem<T>
{
    private T[] items;
    int currentItemCount;
   
    public Heap(int maxHeapSize)
    {
        items = new T[maxHeapSize];
    }

    public void Add(T item)
    {
        item.HeapIndex = currentItemCount;
        items[currentItemCount] = item;
        SortUp(item);
        currentItemCount++;
    }

    public T RemoveFirst()
    {
        T firstItem = items[0];
        currentItemCount--;
        items[0] = items[currentItemCount];
        items[0].HeapIndex = 0;
        SortDown(items[0]);
        return firstItem;

    }

    public bool Contains(T item)
    {
        return Equals(items[item.HeapIndex], item);
    }

    public int Count
    {
        get { return currentItemCount; }
    }
    public void updateItem (T item)
    {
        SortUp(item);
    }
    void SortDown(T item)
    {
        while (true)
        {
            int childindexleft = item.HeapIndex * 2 + 1;
            int childindexright = item.HeapIndex * 2 + 2;
            int swapIndex = 0;

            if (childindexleft < currentItemCount)
            {
                swapIndex = childindexleft;

                if (childindexright< currentItemCount)
                {
                    if(items[childindexleft].CompareTo(items[childindexright]) < 0){
                        swapIndex = childindexright;
                    }
                }

                if (item.CompareTo(items[swapIndex]) < 0)
                {
                    Swap(item, items[swapIndex]);
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }

        }
    }

    void SortUp(T item)
    {
        int parentIndex = (item.HeapIndex - 1) / 2;

        while (true)
        {
            T parentItem = items[parentIndex];
            if(item.CompareTo(parentItem)> 0)
            {
                Swap(item, parentItem);
            }
            else
            {
                break;
            }

            parentIndex = (item.HeapIndex - 1) / 2;

        }
    }
    void Swap(T itemA, T itemB)
    {
        items[itemA.HeapIndex] = itemB;
        items[itemB.HeapIndex] = itemA;

        int itemAindex = itemA.HeapIndex;
        itemA.HeapIndex = itemB.HeapIndex;
        itemB.HeapIndex = itemAindex;
    }

}



public interface IHeapItem<T> :IComparable<T>
{
    int HeapIndex
    {
        get;
        set;
    }
}
