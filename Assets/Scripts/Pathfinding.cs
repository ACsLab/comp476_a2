﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;
using System.Linq;

public class Pathfinding : MonoBehaviour
{
    public enum Heuristic { Dijkstra, Euclidean, cluster }
    CustomGrid grid;
    [SerializeField] private Heuristic _heuristic = Heuristic.Euclidean;
    private HashSet<GridNode> drawingSet = new HashSet<GridNode>();


    private void Awake()
    {
        grid = GetComponent<CustomGrid>();

    }

    // Start is called before the first frame update
    void Start()
    {

    }

   

    public void FindPath(PathRequest request, Action<PathResult> callback)
    {
        //Stopwatch sw = new Stopwatch();
        //sw.Start();

        Vector3[] waypoints = new Vector3[0];
        bool pathSuccess = false;

        GridNode startNode = grid.NodeFromWorldPoint(request.pathStart);
        GridNode targetNode = grid.NodeFromWorldPoint(request.pathEnd);

        if (startNode.walkable && targetNode.walkable)
        {
            Heap<GridNode> openSet = new Heap<GridNode>(grid.MaxSize);
            HashSet<GridNode> closedSet = new HashSet<GridNode>();
            openSet.Add(startNode);

            while (openSet.Count > 0)
            {
                GridNode currentNode = openSet.RemoveFirst();


                closedSet.Add(currentNode);

                if (currentNode == targetNode)
                {
                    //sw.Stop();
                    //print("path found: " + sw.ElapsedMilliseconds + " ms");
                    pathSuccess = true;
                    //drawingSet = closedSet;
                    break;
                }

                foreach (GridNode neighbour in grid.GetNeighbours(currentNode))
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    int newMovementCostToNeighbour = currentNode.Gcost + GetDistance(currentNode, neighbour) + neighbour.movementPenalty;
                    if (newMovementCostToNeighbour < neighbour.Gcost || !openSet.Contains(neighbour))
                    {
                        neighbour.Gcost = newMovementCostToNeighbour;
                        neighbour.hcost = GetDistance(neighbour, targetNode);
                        neighbour.parent = currentNode;

                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                        else
                        {
                            openSet.updateItem(neighbour);
                        }
                    }

                }




            }
            
        }



        if (pathSuccess)
        {
            waypoints = RetracePath(startNode, targetNode);
            pathSuccess = waypoints.Length > 0;

        }

        callback(new PathResult(waypoints, pathSuccess, request.callback));

    }


    //retraces the succesfull path and returns a simpler version of it
    private Vector3[] RetracePath(GridNode startnode, GridNode endnode)
    {
        List<GridNode> path = new List<GridNode>();

        GridNode currentNode = endnode;

        while (currentNode != startnode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);

        return waypoints;

    }

    //only returns points where the direction of the path changes 
    private Vector3[] SimplifyPath(List<GridNode> Path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionold = Vector2.zero;

        for (int i = 1; i < Path.Count; i++)
        {
            Vector2 directionnew = new Vector2(Path[i - 1].gridposition.x - Path[i].gridposition.x, Path[i - 1].gridposition.y - Path[i].gridposition.y);

            if (directionnew != directionold)
            {
                waypoints.Add(Path[i].worldPosition);
            }
            directionold = directionnew;


        }

        return waypoints.ToArray();
    }
    //the heuristic for A* pathfinding 
    private int GetDistance(GridNode nodeA, GridNode nodeB)
    {
        int distX = Mathf.Abs(((int)nodeA.gridposition.x - (int)nodeB.gridposition.x));
        int distY = Mathf.Abs(((int)nodeA.gridposition.y - (int)nodeB.gridposition.y));

        if (distX > distY)
        {
            return 14 * distY + 10 * (distX - distY);
        }

        return (_heuristic != Heuristic.Dijkstra) ? 14 * distX + 10 * (distY - distX) : 0;
    }


    private void OnDrawGizmos()
    {
        foreach(GridNode node in drawingSet)
        {
            Gizmos.color = Color.blue;


           
            Gizmos.DrawCube(node.worldPosition, Vector3.one * (0.6f - 0.1f));
        }
    }
}
