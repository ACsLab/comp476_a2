﻿using System.Collections;
using System.Collections.Generic;

public class Node<TK>
    {
        
        private TK _data;
		private int _index;
        private Node<TK> _parent;
        private List<Node<TK>> _children;
        private int _depth;

        public delegate bool TraversalDataDelegate(TK data);
        public delegate bool TraversalNodeDelegate(Node<TK> node);

        public Node<TK> Parent { get { return _parent; } /*set{ _parent = value;}*/}
        public TK Data { get { return _data; } }
        public List<Node<TK>> Children { get { return _children; } }
        public int Depth { get { return _depth; } }


        public Node(TK data)
        {
            _data = data;
            _children = new List<Node<TK>>();
            _depth = 0;
        }

        public Node(TK data, Node<TK> parent) : this(data)
        {
            _parent = parent;
            _depth = _parent != null ? _parent.Depth + 1 : 0;

        }

        public Node<TK> addchild(TK data)
        {
            //adds the node to the children list 
            Node<TK> node = new Node<TK>(data, this);

            _children.Add(node);
            
            //returns the child node
            return node;
        }

        public Node<TK> addchild(Node<TK> node)
        {
            node._parent = this;
            _children.Add(node);

            return node;
        }
        
        
        public bool HasChildren()
        {
            return _children.Count > 0;
        }

        public Node<TK> FindInChildren(TK data)
        {
            for (int i = 0; i < _children.Count; i++)
            {
                Node<TK> child = _children[i];
                if (child.Data.Equals(data)) return child;
            }
            return null;
        }

        public bool RemoveChild(Node<TK> node)
        {
            return _children.Remove(node);
        }

        public void Traverse(TraversalDataDelegate handler)
        {
            if (handler( _data))
            {
               
                for (int i = 0; i < _children.Count; ++i) _children[i].Traverse(handler);
            }
        }

        public void Traverse(TraversalNodeDelegate handler)
        {
            if (handler(this))
            {
                
                for (int i = 0; i < _children.Count; ++i) _children[i].Traverse(handler);
            }
        }


    }
public class GenericTree<T>
{
    

    

    private Node<T> _rootnode;
    
    private List<Node<T>> _nodes;

    public Node<T> Root { get { return _rootnode; } }
    
    public int Count { get { return _nodes.Count; } }


    public GenericTree(T data)
    {
        _rootnode = new Node<T>(data);
        _nodes = new List<Node<T>>();
        _nodes.Add(_rootnode);
        
    }

    public void addchildto(int index, T data)
    {
        Node<T> newnode = _nodes[index].addchild(data);
        _nodes.Add(newnode);
    }
    //return a node from the tree
    public Node<T> this[int key]
    {
        get { return _nodes[key]; }
    }
   //maybe build tree from array




}
