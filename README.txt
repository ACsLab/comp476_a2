This program implements A* pathfind and the pov graph with the level described in the pdf in the assignment. it also implements the inverse game of tag where multiple entities are chasing a single entity. Each entity has its own independent finite state machine where it attemtps to locate the target for the seekers that would be the one they are chasing for the one being chased that weould be the closest chaser in its visula range. They do this buy casting a ray forward and seeing if it hits another entity if it does it then chacks waht role that entity has and if it should run away or give chase depending on whether or not he is a seeker or an innocent.

The A* is implemented accros a number of files pathrequest manager, pathfinder , customGrid,GridNode, and Heap
starting from the most generic the heap class is a way of storing data in an efficient way so that one does not have to go trhough an entire arrray to search the in this case the openset. it is implemented in a generic way so that it can be reused if need be. The next class is GridNode this class stores all the relevent information a node on the grid would need, its two positions, if it is walkable, and its scores.
custom grid builds grid and determins what areas ar walakable and not walkable. pathfinder is where all the magic happens, it perfomes the A* calculation requested for each entity and and uses the cutomgrid class to retreive information from each node. The path request manager is a static method that handles requests from the path finder appon receiving a request for a new path from an entity it starts a new thread that begins executing the pathfinder code. once complete it sends that information back to the original caller. this way mutiple calls to pathfinder can be made and not slow down the main thread.

the finite state machines

			if near chaser cener of mass or spotted chaser
	[Wander]-----------------------------> [flee]
			<-----------------------------
				far from chasers
				
			spoted target		 in arrival radius
	[Wander]------------->[chase]----------------->[tag]
			<-------------
			lost sight of target